<?php

namespace App;

// Créatioon d'une classe qui contient les différents types de film à la location,
// la location ce faisant plutot sur le type du film plutot que sur le film en lui même , ça me semble plus performant de faire ceci
// au niveau de la cardinalité, nous avons une MovieType OneToMany Movie
class MovieType
{
    private ?array $movies;
    private string $type;
    private int $value;

    public function __construct(?array $movies, string $type, int $value)
    {
        $this->movies = $movies;
        $this->type = $type;
        $this->value = $value;
    }

    public function getMovies(): ?array
    {
        return $this->movies;
    }

    public function addMovie(Movie $movie): \App\MovieType
    {
        if (!in_array($movie, $this->movies))
        {
            $this->movies[] = $movie;
            $movie->setMovieType($this);
        }
        return $this;
    }

    public function removeMovie(Movie $movie): Customer
    {

        if($key = array_search($movie, $this->movies, true))
        {
            unset($this->movies[$key]);
            if ($movie->getMovieType() === $this)
            {
                $movie->setMovieType(null);
            }
        }
        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): void
    {
        $this->type = $type;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     */
    public function setValue(int $value): void
    {
        $this->value = $value;
    }
}
