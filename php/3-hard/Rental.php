<?php

declare(strict_types=1);


namespace App;


class Rental
{
    private Movie $movie;

    private Customer $customer;
    private int $daysRented;

    public function __construct(Movie $movie, int $daysRented, Customer $customer)
    {
        $this->movie = $movie;
        $this->daysRented = $daysRented;
        $this->customer = $customer;
    }

    public function getMovie(): Movie
    {
        return $this->movie;
    }
    /**
     * @param Movie $movie
     */
    public function setMovie(Movie $movie): void
    {
        $this->movie = $movie;
    }

    /**
     * @return int
     */
    public function getDaysRented(): int
    {
        return $this->daysRented;
    }

    /**
     * @param int $daysRented
     */
    public function setDaysRented(int $daysRented): void
    {
        $this->daysRented = $daysRented;
    }

    /**
     * @return Customer
     */
    public function getCustomer(): Customer
    {
        return $this->customer;
    }

    public function setCustomer(?Customer $customer): \App\Rental
    {
        $this->customer = $customer;
        return $this;
    }
}