<?php

declare(strict_types=1);


namespace App;
require_once('MovieType.php');
require_once('Movie.php');
require_once('Rental.php');

class Customer
{
    //Ajout des parametres
    private string $name;
    private array $rentals;
    public function __construct(String $name, array $rentals)
    {
        $this->name = $name;
        $this->rentals = $rentals;
    }
    public function getName(): string
    {
        return $this->name;
    }
    //ajout de tout les getters/setters
    public function setName(string $name): void
    {
        $this->name = $name;
    }
    public function getRentals(): array
    {
        return $this->rentals;
    }
    public function addRental(Rental $rental): Customer
    {
        if (!in_array($rental, $this->rentals))
        {
            $this->rentals[] = $rental;
            $rental->setCustomer($this);
        }
        return $this;
    }

    public function removeRental(Rental $rental): Customer
    {

        if($key = array_search($rental, $this->rentals, true))
        {
            unset($this->rentals[$key]);
            if ($rental->getCustomer() === $this)
            {
                $rental->setCustomer(null);
            }
        }
        return $this;
    }


    public function statement(): string {
        $totalAmount = 0.0;
        $frequentRenterPoints = 0;
        $result = "Rental Record for " . $this->getName() . "\n";

        foreach ($this->getRentals() as $rental)
        {
            $thisAmount = 0.0;
            // determine le montant pour chaque type de film
            $priceCode = $rental->getMovie()->getPriceCode();
            switch($rental->getMovie()->getMovieType()->getType())
            {
                case 'regular':
                    $thisAmount += $priceCode;
                    if($rental->getDaysRented() > 2)
                        $thisAmount += ($rental->getDaysRented() - 2) * 1.5;
                    break;
                case 'new release':
                    $thisAmount += $rental->getDaysRented() * 3;
                    if ($rental->getDaysRented() > 1)
                    {
                        $frequentRenterPoints++;
                    }
                    break;
                case 'children':
                    echo 'test3';
                    $thisAmount += $priceCode;
                    if($rental->getDaysRented() > 3)
                    {
                        $thisAmount += ($rental->getDaysRented() - 3) * 1.5;
                    }
                    break;
            }

            $frequentRenterPoints++;
            $result .= "\t" . $rental->getMovie()->getTitle() . "\t"
                . number_format($thisAmount, 1) . "\n";
            $totalAmount += $thisAmount;
        }
        $result .= "You owed " . number_format($totalAmount, 1)  . "\n";
        $result .= "You earned " . $frequentRenterPoints . " frequent renter points\n";

        echo $result;
        return $result;
    }
}
//test

//$movieType1 = new MovieType(null,'regular' , 2);
//$movieType2 = new MovieType(null,'new release' , 40);
//$movie1 = new Movie('Le meilleur film du monde',  $movieType1);
//$movie2 = new Movie('Le meilleur film du monde2',  $movieType2);
//$customer = new Customer('quentin', []);
//$rental1 = new Rental($movie1, 4, $customer);
//$rental2 = new Rental($movie2, 1, $customer);
//$customer->addRental($rental1)->addRental($rental2);
//$customer->statement();