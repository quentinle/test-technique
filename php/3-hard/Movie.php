<?php

declare(strict_types=1);


namespace App;


class Movie
{
    public const CHILDREN = 2;
    public const REGULAR = 0;
    public const NEW_RELEASE = 1;

    private string $title;
    private ?MovieType $movieType;

    public function __construct(string $title, ?MovieType $movieType = null)
    {
        $this->title = $title;
        $this->movieType = $movieType;
    }

    // deletion du priceCode , le get permet
    public function getPriceCode(): int
    {
        return $this->movieType->getValue();
    }
    public function getTitle(): string
    {
        return $this->title;
    }
    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return MovieType
     */
    public function getMovieType(): ?MovieType
    {
        return $this->movieType;
    }

    public function setMovieType(?MovieType $movieType = null): void
    {
        $this->movieType = $movieType;
    }
}