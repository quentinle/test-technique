<?php

declare(strict_types=1);

namespace App;

class Rover
{
    private string $direction;
    private int $y;
    private int $x;

    // création d'une matrice, qui , selon la direction courante, défini la direction d'une rotation, ainsi que sa valeur à transformer
    // sur les axes X/Y
    const DIRECTIONCHANGE   = [
        'N' => [
            'L' => 'W',
            'R' => 'E',
            'f' => 1
        ],
        'E' => [
            'L' => 'N',
            'R' => 'S',
            'f' => -1
        ],
        'W' => [
            'L' => 'S',
            'R' => 'N',
            'f' => 1
        ],
        'S' => [
            'L' => 'E',
            'R' => 'W',
            'f' => -1
        ],
    ];

    public function __construct(int $x, int $y, string $direction)
    {
        $this->direction = $direction;
        $this->y = $y;
        $this->x = $x;
    }
    public function receive(string $commandsSequence): void
    {
        for ($i = 0; $i <  strlen($commandsSequence); ++$i)
        {
            $command = substr($commandsSequence, $i, 1);
            if ($command === "l" || $command === "r")
            {
                // Rotate Rover
                $this->direction = self::DIRECTIONCHANGE[$this->direction][$command];
            } else {


                if ($command === "f") {
                    if ($this->direction === "N" || $this->direction ==='S')
                    {
                        $this->y +=  self::DIRECTIONCHANGE[$this->direction][$command];
                    }else
                    {
                        $this->x +=  self::DIRECTIONCHANGE[$this->direction][$command];
                    }
                }
            }
        }
    }
}