<?php

function convertSize2($bytes, $precision = 2): string
{
    $units = array('B', 'KB', 'MB', 'GB', 'TB', 'PT', 'EB', 'ZB', 'YB', 'HB');
    // on cherche l'exponentielle à laquelle il faut élever 1024 pour obtenir le nombre de bytes
    $base = log($bytes, 1024);
    // ainsi on obtient le nombre de fois que 1024 doit être utilisé pour donner $bytes
    // pour déterminer l'index de l'unité à utiliser dans le tableau $units
    // la taille en octets est ensuite divisée par la puissance de 1024 correspondante pour obtenir la taille en unités.

    return round(pow(1024, $base - floor($base)), $precision) .' '. $units[floor($base)];
}


// test
$bytes = 2500000;
echo convertSize2($bytes);
echo $base = log($bytes, 1024) . "\n";
echo round(pow(1024, $base - floor($base)), 2);
//echo convertSize1($bytes) . "\n";
//echo $pow = floor((log($bytes)) / log(1024)) . "\n";
//echo $pow = min($pow, 10 - 1) . "\n";
//echo $bytes /= pow(1024, $pow);
//echo log(1024, 10);
