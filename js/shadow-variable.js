const articleList = []; // In a real app this list would be full of articles.
//const plutot que var , est changement de nom pour être plus claire
const nbKudos = 5;


// fonctionnalités ES6
const calculateTotalKudos = (articles) => {
    let kudos = 0;

    for (let article of articles) {
        kudos += article.kudos;
    }

    return kudos;
};

document.write(`
  <p>Maximum kudos you can give to an article: ${nbKudos}</p>
  <p>Total Kudos already given across all articles: ${calculateTotalKudos(articleList)}</p>
`);